*** Settings ***
Library    SeleniumLibrary
Library  OperatingSystem
Library  DateTime

*** Variables ***
 ${BROWSER}  chrome
 ${ROOT}  http://blazedemo.com/
 ${ORIGEN}  Paris
 ${DESTINO}  London
 ${TYPE OF FILE}  png
*** Keywords ***
Un usuario accede a la plataforma
    Open browser    http://blazedemo.com/   chrome

Introduce un Destino "${DESTINO}"
    Select From List by Value   xpath://select[@name='toPort']    ${DESTINO}

Si ambos son validos se obtiene una ruta valida
   ${Date}=  Get DateTime
   Click Button    css:input[type='submit']
   @{flights}=  Get WebElements    css:table[class='table']>tbody tr
   Do Screenshot  bdd_${Date}
   Should Not Be Empty     ${flights}

Close all test browsers
    Close all browsers

Get DateTime
  ${date1}=  Get Current Date  result_format=%Y-%m-%d %H-%M-%S
  [Return]   ${date1}

Do Screenshot
  [Arguments]  ${filename}
  Capture Page Screenshot  ${filename}.${TYPE OF FILE}
  Move File   ${filename}.${TYPE OF FILE}  ./screenshots/
  Log To Console  ${\n}Screenshot

*** Test Cases ***
Valid Login
    [Tags]  bddweb    Given Un usuario accede a la plataforma
    When Introduce un Destino "${DESTINO}"
    Then Si ambos son validos se obtiene una ruta valida