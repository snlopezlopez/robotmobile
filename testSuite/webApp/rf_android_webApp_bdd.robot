*** Settings ***
Documentation    Suite description
Library  AppiumLibrary
Library  DateTime
Library  OperatingSystem

*** Variables ***
${TYPE OF FILE}  png
${ANDROID_PLATFORM_NAME}  Android
${PAGE}    https://www.atsistemas.com/es
${SEARCH_TEXT}  atsistemas

*** Keywords ***
Get DateTime
  ${date1}=  Get Current Date  result_format=%Y-%m-%d %H-%M-%S
  [Return]   ${date1}

Do Screenshot
  [Arguments]  ${filename}
  Capture Page Screenshot  ${filename}.${TYPE OF FILE}
  Move File   ${filename}.${TYPE OF FILE}  ./screenshots/
  Log To Console  ${\n}Screenshot

Un usuario accede a la plataforma
     Open Application  http://127.0.0.1:4723/wd/hub  platformName=${ANDROID_PLATFORM_NAME}
          ...  browserName=Chrome   newCommandTimeout=60
     Switch To Context  CHROMIUM
     Go To Url  ${PAGE}

Introduce un valor de búsqueda "${SEARCH_TEXT}"

     Wait Until Element Is Visible  //button
     Click Element  //button
     Input Text   //input[@id='queryStrHome' and @class='pull-left']  ${SEARCH_TEXT}\n
     Wait Until Element Is Visible   //div/p[@class='p-resultados']


Se comprueba que arroja un resultado

     ${listresults}=  Get Matching Xpath Count  //li[@class='search-item']
     Should Be True   len($listresults) > 0
     ${Date}=  Get DateTime
     Do Screenshot  webApp_${Date}
     Quit Application


*** Test Cases ***
Comprobar busqueda en web app
    [Tags]  bdd
    Given Un usuario accede a la plataforma
    When Introduce un valor de búsqueda "${SEARCH_TEXT}"
    Then Se comprueba que arroja un resultado