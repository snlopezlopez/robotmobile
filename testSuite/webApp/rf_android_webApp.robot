*** Settings ***
Documentation    Suite description
Library  AppiumLibrary
Library  DateTime
Library  OperatingSystem

*** Variables ***
${TYPE OF FILE}  png
${ANDROID_PLATFORM_NAME}      Android
${PAGE}    https://www.atsistemas.com/es
${SEARCH_TEXT}  atsistemas

*** Keywords ***
Open Android Chrome
     Open Application  http://127.0.0.1:4723/wd/hub  platformName=${ANDROID_PLATFORM_NAME}
          ...  browserName=Chrome   newCommandTimeout=60
     Switch To Context  CHROMIUM

Search Text on AT Page
     ${Date}=  Get DateTime
     Wait Until Element Is Visible  //button
     Click Element  //button
     Input Text   //input[@id='queryStrHome' and @class='pull-left']  ${SEARCH_TEXT}\n
     Wait Until Element Is Visible   //div/p[@class='p-resultados']
     ${listresults}=  Get Matching Xpath Count  //li[@class='search-item']
     Should Be True   len($listresults) > 0
     Do Screenshot  webApp_${Date}
     Quit Application

Get DateTime
  ${date1}=  Get Current Date  result_format=%Y-%m-%d %H-%M-%S
  [Return]   ${date1}

Do Screenshot
  [Arguments]  ${filename}
  Capture Page Screenshot  ${filename}.${TYPE OF FILE}
  Move File   ${filename}.${TYPE OF FILE}  ./screenshots/
  Log To Console  ${\n}Screenshot

*** Test Cases ***
Execute Chrome Responsive
    [Documentation]  Execute Chrome Android
    [Tags]    responsive
    Open Android Chrome
    Go To Url  ${PAGE}
    Click Element   //div[@id='divBtnsCookies']
    Search Text on AT Page